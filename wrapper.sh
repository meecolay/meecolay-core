#!/bin/sh

export MEE_ROOT=$(dirname $(readlink -f "$0"))
export MEE_APPID="$1"
. "$MEE_ROOT/env.sh"

cd "$(dirname "$2")"
exec "$2"

