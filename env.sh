if [ -n "$MEE_ROOT" ]; then
	export MEE_ROOT
else
	# This needs to improve, but works well enough for debugging.
	# wrapper.sh will set a hardcoded /opt path.
	export MEE_ROOT=$(pwd)
fi

if [ -n "$PATH" ]; then
	export PATH="$MEE_ROOT/bin:$PATH"
else
	export PATH="$MEE_ROOT/bin"
fi

if [ -n "$LD_LIBRARY_PATH" ]; then
	export LD_LIBRARY_PATH="$MEE_ROOT/lib:/opt/qtm12/lib:$LD_LIBRARY_PATH"
else
	export LD_LIBRARY_PATH="$MEE_ROOT/lib:/opt/qtm12/lib"
fi

#if [ -n "$LD_PRELOAD" ]; then
#	export LD_PRELOAD="$MEE_ROOT/lib/libstdc++.so.6:$LD_PRELOAD"
#else
#	export LD_PRELOAD="$MEE_ROOT/lib/libstdc++.so.6"
#fi


if [ -n "$QML_IMPORT_PATH" ]; then
	export QML_IMPORT_PATH="/opt/qtm12/imports:$QML_IMPORT_PATH"
else
	export QML_IMPORT_PATH="/opt/qtm12/imports"
fi
